import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the ApiCloudVisionProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiCloudVisionProvider {
 //Apikey de google cloud vision
 googleCloudVisionAPIKey = "AIzaSyAJ0mZu4kIv_rfO9JvOpaFF4thkRWOojL0";
  constructor(public http: Http) {
    console.log('Hello ApiCloudVisionProvider Provider');
  }

  //Funcion para hacer la petición a google cloud vision, estructura necesaria para la petición segun la documentación
  detectPeople(base64) {
    const body = {
      "requests": [
        {
          "image": {
            "content": base64
          },
          "features": [
            {
              "type": "OBJECT_LOCALIZATION"
            }
          ]
        }
      ]
    }
    //Retornar la respuesta
    return this.http.post(`https://vision.googleapis.com/v1/images:annotate?key=${this.googleCloudVisionAPIKey}`, body)
  }

}
