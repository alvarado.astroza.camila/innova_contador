import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
/*
  Generated class for the DbFirebaseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DbFirebaseProvider {
  apiUrl = 'http://34.70.121.7:8080';
  
  constructor(public http: Http) {
    console.log('Hello DbFirebaseProvider Provider');
  }

  addMedida(data) {
    var headers = new Headers();
    // headers.append('Access-Control-Allow-Origin' , '*');
    // headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
    // headers.append('Accept','application/json');
    headers.append('content-type','application/json');
    var options = new RequestOptions ({headers: headers}); 
    var res;
    try {
      this.http.put(this.apiUrl, JSON.stringify(data))
      .subscribe(res => {
        res = res
      }, (err) => {
        res = err;
      });
    }
    catch (error) {
      res = error;
    }
    return res;
  }
}
